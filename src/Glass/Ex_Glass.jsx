import React, { Component } from "react";
import { glassData } from "./data.jsx";
import Glass_List from "./List_Glass.jsx";
import Glass_Model from "./Glass_Model.jsx";

export default class Ex_Glass extends Component {
  state = {
    glassArr: glassData,
    glassDetail: glassData[0],
  };

  handleShowGlassDetail = (glassID) => {
    let index = this.state.glassArr.findIndex((item) => {
      return item.id == glassID;
    });
    index !== -1 &&
      this.setState(
        {
          glassDetail: this.state.glassArr[index],
        },
        () => {
          console.log(this.state.glassArr[index]);
        }
      );
  };

  render() {
    return (
      <div>
        <Glass_Model glassDetail={this.state.glassDetail} />
        <div className="row">
          {this.state.glassArr.map((item) => {
            return (
              <Glass_List
                detail={item}
                handleShowGlassDetail={this.handleShowGlassDetail}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
