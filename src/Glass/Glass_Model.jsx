import React, { Component } from "react";
import styles from "./Layout.module.css";

export default class Glass_Model extends Component {
  render() {
    let { name, price, desc, url } = this.props.glassDetail;
    return (
      <div
        className="card bg-dark text-white"
        style={{ width: "20rem", margin: "auto" }}
      >
        <img
          className="card-img"
          src="../glassesImage/model.jpg"
          alt="Card image"
        />
        <div className={styles.glassOverlay}>
          <img className="card-img" src={url} alt="" />
        </div>
        <div className={`${styles["my-card-overlay"]}`}>
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{desc}</p>
          <p className="card-text">Price: {price} $</p>
        </div>
      </div>
    );
  }
}
